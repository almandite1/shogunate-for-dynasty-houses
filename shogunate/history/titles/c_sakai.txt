c_nmih_sakai = {

	1.1.1={
		effect={
			title:b_nmih_sakai1={
				set_title_name=b_nmih_sakai1
			}
		}
	}
	1100.1.1={
		effect={
			title:b_nmih_sakai1={
				set_title_name=b_nmih_sakai9
			}
		}
	}
	1331.1.1={
		effect={
			title:b_nmih_sakai1={
				set_title_name=b_nmih_sakai1
			}
		}
	}

	1.1.1 = { change_development_level = 40 }
	1100.1.1 = { change_development_level = 40 }
	1331.1.1 = { change_development_level = 40 }

	0701.1.1={
		liege="d_nmih_echizen"
	}
	1180.1.1={
		liege=0
		holder=10131105 # Hikita Toshinobu
	}
	1181.7.26={
		liege="d_nmih_kaga" # Battle of Yokota-gawara
	}
	1183.5.20={
		liege="d_nmih_echizen" # Siege of Hiuchi Castle
		holder=10110032 # Taira Norimori
	}
	1183.6.22={
		liege=0
		holder=0 # Battle of Shinohara
	}
	1459.9.7={
		liege="d_nmih_yamato"
		holder=11000774 # Kujo Kyokaku
	}
	1471.8.1={
		liege="k_nmih_honganji"
		holder=10042201 # Honganji Rennyo
	}
	1475.8.1={
		liege="d_nmih_echizen"
		holder=10030170 # Horie Kagemochi
	}
	1490.1.1={
		holder=10030171 # Horie Kagezane
	}
	1530.1.1={
		holder=10030173 # Horie Kagetada
	}
	1573.9.16={
		liege="k_nmih_chubu"
		holder=10029120 # Oda Nobunaga
	}
	1573.9.24={
		liege="d_nmih_echizen" # 30100 Maeba Yoshitsugu
		holder=10030130 # Mizoe Kageyasu
	}
	1574.4.20={
		holder=10042324 # Shimotsuma Raisho
	}
	1575.9.25={
		holder=10029120 # Oda Nobunaga
	}
	1575.10.1={
		liege="d_nmih_echizen" # 29280 Shibata Katsuie
		holder=10029280 # Shibata Katsuie
	}

}
