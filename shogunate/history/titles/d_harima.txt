d_nmih_harima = {
	0701.1.1={
		liege="e_nmih_tenno"
	}
	1179.12.18={
		liege = "k_nmih_sanyo"
		holder=10110070 # Taira Yukimori
	}
	1181.3.20={
		liege = "e_nmih_japan"
	}
	1183.8.17={
		liege=0
		holder=0 # Taira's escape to the west
	}
	1336.1.1={
		liege="e_nmih_japan"
		holder=10053500 # Akamatsu Norimura
	}
	1350.2.18={
		holder=10053501 # Akamatsu Norisuke
	}
	1351.5.4={
		holder=10053503 # Akamatsu Norisuke
	}
	1372.1.13={
		holder=10053507 # Akamatsu Yoshinori
	}
	1427.10.11={
		holder=10053511 # Akamatsu  Mitsusuke
	}
	1441.9.25={
		holder=10050039 # Yamana Mochitoyo
	}
	1454.1.1={
		holder=10050040 # Yamana Noritoyo
	}
	1458.1.1={
		holder=10050039 # Yamana Mochitoyo
	}
	1467.6.27={ # Onin war
		liege = "e_nmih_japan"
		holder=10053518 # Akamatsu Masanori
	}
	1477.12.16={ # End of Onin war
		liege = 0
	}
	1496.6.6={
		holder=10053519 # Akamatsu Yoshimura
	}
	1520.11.1={
		holder=10053502 # Akamatsu Harumasa
	}
	1558.8.1={
		holder=10053533 # Akamatsu Yoshisuke
	}
	1570.1.1={
		holder=10053536 # Akamatsu Norifusa
	}
	1577.10.1={
		liege="e_nmih_japan"
		holder=10029400 # Hashiba Hideyoshi
	}
	1582.6.21={ # Honno-ji Incident
		liege="k_nmih_sanyo"
	}
}
