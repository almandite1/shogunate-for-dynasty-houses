c_nmih_miyako = {

	1.1.1={
		effect={
			title:b_nmih_miyako1={
				set_title_name=b_nmih_miyako9
			}
		}
	}
	1100.1.1={
		effect={
			title:b_nmih_miyako1={
				set_title_name=b_nmih_miyako9
			}
		}
	}
	1185.4.25={
		effect={
			title:b_nmih_miyako1={
				set_title_name=B_NMIH_MIYAKO9_1185
			}
		}
	}

	1.1.1 = { change_development_level = 10 }
	1100.1.1 = { change_development_level = 10 }
	1180.1.1 = { change_development_level = 20 }

	100.1.1={
	}
	180.1.1={
		holder=10290001 # Himiko
	}
	1180.1.1={
		liege="d_nmih_buzen"
		holder=12750500 # Itai Taneto
	}
	1185.3.4={
		liege="k_nmih_western_chugoku" # Battle of Ashiya-no-ura
	}
	1185.4.25={
		liege=0
		holder=0 # Battle of Dan-no-ura
	}
	1400.1.17={
		liege="k_nmih_western_chugoku"
		holder=10067207 # Sugi Shigesada
	}
	1410.1.1={
		holder=10067260 # Sugi Shigetsuna
	}
	1440.1.1={
		holder=10067261 # Sugi Shigekuni
	}
	1460.1.1={
		holder=10067262 # Sugi Shigetomo
	}
	1500.1.1={
		holder=10067263 # Sugi Shigekiyo
	}
	1520.1.1={
		holder=10067264 # Sugi Shigenori
	}
	1551.9.30={
		liege=0
		holder=10067265 # Sugi Shigesuke
	}
	1557.4.3={
		holder=10067266 # Sugi Shigeyoshi
	}
	1557.5.1={
		liege="k_nmih_western_chugoku"
	}
	1579.1.1={
		liege="d_nmih_bungo"
		holder=10067104 # Nagano Sukemori
	}

}
