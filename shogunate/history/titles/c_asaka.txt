c_nmih_asaka = {

	1.1.1={
		effect={
			title:b_nmih_asaka1={
				set_title_name=b_nmih_asaka5
			}
		}
	}
	1100.1.1={
		effect={
			title:b_nmih_asaka1={
				set_title_name=b_nmih_asaka9
			}
		}
	}
	1189.11.15={
		effect={
			title:b_nmih_asaka1={
				set_title_name=b_nmih_asaka1
			}
		}
	}

	1.1.1 = { change_development_level = 30 }
	1100.1.1 = { change_development_level = 30 }
	1189.11.15 = { change_development_level = 30 }

	100.1.1={
	}
	1180.1.1={
		liege="k_nmih_northern_oshu"
		holder=12060060 # Sato Motoharu
	}
	1189.9.21={
		liege="k_nmih_western_kanto" # Battle of Mountain Atsukashi
		holder=10120075 # Minamoto Yoritomo
	}
	1189.11.15={
		holder=12220200 # Nikaido Yukimasa
	}
	1205.1.1={
		holder=12220201 # Nikaido Yukimitsu
	}
	1219.10.17={
		holder=12220204 # Nikaido Yukimori
	}
	1253.12.8={
		holder=12220205 # Nikaido Yukitsuna
	}
	1281.6.7={
		holder=12220240 # Nikaido Yoristuna
	}
	1283.10.24={
		holder=12220243 # Nikaido Sadatsuna
	}
	1331.1.1={
		holder=12220245 # Nikaido Yukitomo
	}
	1353.10.22={
		holder=12220250 # Nikaido Yukimitsu
	}
	1380.1.1={
		holder=12220252 # Nikaido Yukinaga
	}
	1400.1.1={
		holder=12220254 # Nikaido Yukimune
	}
	1420.1.1={
		holder=12220256 # Nikaido Yukitsugu
	}
	1443.1.1={
		liege=0
		holder=10006805 # Nikaido Tameuji
	}
	1464.8.5={
		holder=10006804 # Nikaido Yukimitsu
	}
	1474.2.11={
		holder=10006803 # Nikaido Yukiaki
	}
	1495.1.1={
		holder=10006837 # Nikaido Yukikage
	}
	1504.1.1={
		holder=10006801 # Nikaido Haruyuki
	}
	1542.7.2={
		holder=10006802 # Nikaido Teruyuki
	}
	1564.10.22={
		holder=10006838 # Nikaido Moriyoshi
	}
	1566.1.1={
		liege="d_nmih_aizu" # vassal of Ashina
	}
	1581.9.23={
		holder=10006840 # Nikaido Yukichika
	}
	1582.1.1={
		holder=10006841 # Nikaido Yukihide
	}
}
