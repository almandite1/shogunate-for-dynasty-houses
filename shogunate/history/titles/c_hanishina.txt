c_nmih_hanishina = {

	1.1.1={
		effect={
			title:b_nmih_hanishina1={
				set_title_name=b_nmih_hanishina2
			}
		}
	}
	1100.1.1={
		effect={
			title:b_nmih_hanishina1={
				set_title_name=b_nmih_hanishina4
			}
		}
	}
	1210.1.1={
		effect={
			title:b_nmih_hanishina1={
				set_title_name=B_NMIH_HANISHINA4_1210
			}
		}
	}
	1390.1.1={
		effect={
			title:b_nmih_hanishina1={
				set_title_name=b_nmih_hanishina1
			}
		}
	}
	1560.1.1={
		effect={
			title:b_nmih_hanishina1={
				set_title_name=B_NMIH_HANISHINA1_1560
			}
		}
	}

	1.1.1 = { change_development_level = 20 }
	1100.1.1 = { change_development_level = 20 }
	1390.1.1 = { change_development_level = 20 }

	0701.1.1={
		liege="d_nmih_shinano"
	}
	1160.1.1={
		liege="d_nmih_shinano"
		holder=12385000 # Murakami Tamekuni
	}
	1180.6.15={
		liege="k_nmih_shinano" # Minamoto Yoshinaka
		holder=12385013 # Murakami Tsunenari
	}
	1183.6.22={
		liege="k_nmih_hokuriku"
	}
	1183.8.17={
		liege="e_nmih_japan"
	}
	1184.1.3={
		liege="k_nmih_western_kanto" # Minamoto Yoritomo
	}
	1185.4.25={
		liege="d_nmih_shinano"
	}
	1210.1.1={
		holder=10131653 # Hojo Yoshitoki
	}
	1224.7.1={
		holder=10131668 # Hojo Shigetoki
	}
	1261.11.26={
		holder=10131878 # Hojo Yoshimune
	}
	1277.4.1={
		holder=10131714 # Hojo Yoshimasa
	}
	1282.1.8={
		holder=10131888 # Hojo Kunitoki
	}
	1333.7.4={
		liege="e_nmih_tenno"
		holder=10041075 # Yamato Godaigo
	}
	1334.1.1={
		liege="d_nmih_shinano"
		holder=12360710 # Ogasawara Sadamune
	}
	1337.1.1={
		holder=12385041 # Murakami Nobusada
	}
	1350.1.1={
		holder=12385045 # Murakami Morokuni
	}
	1390.1.1={
		holder=12385050 # Murakami Mitsunobu
	}
	1410.1.1={
		holder=12385055 # Murakami Mochikiyo
	}
	1435.1.1={
		liege=0
		holder=12385060 # Murakami Yorikiyo
	}
	1460.1.1={
		holder=12385085 # Murakami Mitsukiyo
	}
	1470.1.1={
		holder=10021502 # Murakami Masakuni
	}
	1494.1.1={
		liege=0
		holder=10021501 # Murakami Akikuni
	}
	1528.1.1={
		liege=0
		holder=10021500 # Murakami Yoshikiyo
	}
	1553.9.1={
		liege="d_nmih_kai" # Takeda Shingen
		holder=10020010 # Takeda Shingen
	}
	1556.1.1={
		holder=12380636 # Kosaka Muneshige
	}
	1559.1.1={
		liege="d_nmih_shinano"
	}
	1561.9.1={
		liege="d_nmih_shinano" # 20010 Takeda Shingen
		holder=10020080 # Kasuga Toratsuna
	}
	1578.6.12={
		holder=10020082 # Kasuga Masamoto
	}
	1582.4.3={
		liege="e_nmih_japan"
		holder=10029120 # Oda Nobunaga
	}
	1582.04.15={
		liege="k_nmih_chubu" # 29160 Oda Nobutada
		holder=10025421 # Mori Nagayoshi
	}
	1582.6.21={ # Honno-ji Incident
		liege=0
		holder=10020082 # Kasuga Masamoto
	}
	1582.7.13={
		liege="d_nmih_echigo" # Uesugi Kagekatsu
		holder=10021503 # Murakami Kunikiyo
	}

}
