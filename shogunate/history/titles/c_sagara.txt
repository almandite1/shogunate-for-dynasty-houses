c_nmih_sagara = {

	1.1.1={
		effect={
			title:b_nmih_sagara1={
				set_title_name=b_nmih_sagara2
			}
		}
	}
	1331.1.1={
		effect={
			title:b_nmih_sagara1={
				set_title_name=b_nmih_sagara1
			}
		}
	}
	1400.1.1={
		effect={
			title:b_nmih_sagara1={
				set_title_name=b_nmih_sagara4
			}
		}
	}

	1.1.1 = { change_development_level = 10 }
	1331.1.1 = { change_development_level = 30 }
	1400.1.1 = { change_development_level = 30 }

	0701.1.1={
		liege="d_nmih_yamashiro"
	}
	1180.6.15={
		liege="k_nmih_yamashiro" # Prince Mochihito
		holder=10041091 # Prince Mochihito
	}
	1180.6.20={
		liege="e_nmih_japan" # Taira Kiyomori
		holder=10110025 # Taira Kiyomori. Battle of Uji
	}
	1181.3.20={
		holder=10110042 # Taira Munemori
	}
	1183.8.17={
		holder=10120084 # Minamoto Yoshinaka. Taira's escape to the west
	}
	1184.3.4={
		liege=0
		holder=0 # Battle of Awazu
	}
	1442.9.8={
		liege="k_nmih_kanrei"
		holder=10040543 # Hosokawa Katsumoto
	}
	1467.6.27={
		liege="k_nmih_kanrei"
		holder=10045860 # Koma YamashiroNoKami
	}
	1477.10.1={
		liege="k_nmih_western_chugoku"
		holder=10057007 # Ouchi Masahiro
	}
	1477.12.16={
		liege="d_nmih_yamashiro"
		holder=10048121 # Hatakeyama Masanaga
	}
	1482.12.1={
		liege="d_nmih_kawachi"
		holder=10048115 # Hatakeyama Yoshinari
	}
	1485.12.11={
		liege=0 # Yamashiro Kuni-Ikki
		holder=10045860 # Koma YamashiroNoKami
	}
	1493.8.18={
		liege="d_nmih_kawachi"
		holder=10048902 # Furuichi Choin
	}
	1496.1.1={
		liege="k_nmih_kanrei"
		holder=10040800 # Akazawa Tomotsune
	}
	1507.8.4={
		holder=10040802 # Akazawa Nagatsune
	}
	1508.9.7={
		liege="d_nmih_yamashiro"
		holder=10057001 # Ouchi Yoshioki
	}
	1518.9.6={
		liege="k_nmih_kanrei"
		holder=10040566 # Hosokawa Takakuni
	}
	1527.3.14={
		holder=10042082 # Yanagimoto Kataharu
	}
	1530.8.2={
		holder=10040901 # Yanagimoto Jinjiro
	}
	1532.1.1={
		holder=10040500 # Hosokawa Harumoto
	}
	1534.1.1={
		holder=10040831 # Kizawa Nagamasa
	}
	1542.4.2={
		holder=10040912 # Takabatake Naganao
	}
	1549.7.18 ={
		liege="k_nmih_southern_kinai"
		holder=10059007 # Miyoshi Nagayoshi
	}
	1555.1.1={
		holder=10059006 # Miyoshi Nagayasu
	}
	1567.5.24={
		liege="d_nmih_yamashiro"
	}
	1568.1.1={
		holder=10059026 # Miyoshi Masakatsu
	}
	1568.10.19={
		liege="d_nmih_yamashiro"
		holder=10029120 # Oda Nobunaga
	}
	1568.11.7={
		liege="e_nmih_japan"
		holder=10040042 # Ashikaga Yoshiaki
	}
	1573.8.25={
		liege="k_nmih_chubu"
		holder=10029120 # Oda Nobunaga
	}
	1574.5.1={
		holder=10029390 # Ban Naomasa
	}
	1575.12.9={
		liege="e_nmih_japan"
	}
	1576.5.30={
		holder=10025632 # Akechi Mitsuhide
	}
	1582.6.21={
		liege="d_nmih_yamashiro"
		holder=10025632 # Akechi Mitsuhide
	}
	1582.7.2={
		liege="d_nmih_yamashiro"
		holder=10029400 # Hashiba Hideyoshi
	}
	1582.8.1={
		holder=10029439 # Asano Nagayoshi
	}
}
