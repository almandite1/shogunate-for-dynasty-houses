c_nmih_yabu = {

	1.1.1={
		effect={
			title:b_nmih_yabu1={
				set_title_name=b_nmih_yabu9
			}
		}
	}
	1100.1.1={
		effect={
			title:b_nmih_yabu1={
				set_title_name=b_nmih_yabu9
			}
		}
	}
	1331.1.1={
		effect={
			title:b_nmih_yabu1={
				set_title_name=B_NMIH_YABU9_1331
			}
		}
	}

	1.1.1 = { change_development_level = 10 }
	1100.1.1 = { change_development_level = 10 }
	1423.1.1 = { change_development_level = 20 }

	100.1.1={
	}
	200.1.1={
		holder=10290001 # Himiko
	}
	250.1.1={
		holder=10290005 # Toyo
	}
	1180.1.1={
		liege="d_nmih_chikuzen"
		holder=12751200 # Umi Shigezane
	}
	1185.3.4={
		liege=0
	}
	1186.1.1={
		liege=0
		holder=0 # Battle of Ashiya-no-ura
	}
	1396.1.1={
		liege="d_nmih_hizen"
		holder=10057612 # Shibukawa Mitsuyori
	}
	1419.1.1={
		holder=10057618 # Shibukawa Yoshitoshi
	}
	1428.1.1={
		holder=10057614 # Shibukawa Mitsunao
	}
	1434.1.1={
		holder=10057615 # Shibukawa Norinao
	}
	1479.1.1={
		holder=10057623 # Shibukawa Manjumaru
	}
	1487.1.1={
		holder=10057601 # Shibukawa Tadashige
	}
	1504.1.1={
		liege="d_nmih_hizen"
		holder=10068000 # Shoni Sukemoto
	}
	1536.9.19={
		holder=10068003 # Shoni Fuyuhisa
	}
	1559.2.18={
		liege="k_nmih_western_chugoku"
		holder=10067700 # Tsukushi Korekado
	}
	1564.8.1={
		liege="d_nmih_bungo"
	}
	1567.1.1={
		holder=10067704 # Tsukushi Hirokado
	}
	1578.12.10={
		liege="d_nmih_hizen"
	}

}
