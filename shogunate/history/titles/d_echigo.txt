d_nmih_echigo = {
	0697.1.1={
		liege="e_nmih_tenno"
	}
	0891.1.1={
		holder=0 #  
	}
	0984.3.11={
		holder=11003597 # Fujiwara Arikuni {Echigo no Kami}
	}
	1025.4.30={
		holder=11003338 # Fujiwara Takasuke {Echigo no Kami}
	}
	1029.2.16={
		holder=0 #  
	}
	1038.1.14={
		holder=11005735 # Fujiwara Kaneyasu {Echigo no Kami}
	}
	1042.1.1={
		holder=0 #  
	}
	1137.2.28={
		holder=11006757 # Fujiwara Ieaki {Echigo no Kami}
	}
	1145.1.19={
		holder=11006758 # Fujiwara Narichika {Echigo no Kami}
	}
	1147.2.8={
		holder=11006756 # Fujiwara Takasue {Echigo no Kami}
	}
	1155.3.10={
		holder=11006758 # Fujiwara Narichika {Echigo no Kami}
	}
	1166.9.30={
		holder=10110660 # Taira Tokizane {Echigo no Kami}
	}
	1180.1.1={
		liege = 0 # De facto
		holder=12270130 # Jo [Taira] Sukenaga
	}
	1181.3.11={
		holder=12270131 # Jo [Taira] Nagamochi {Echigo no Kami}
	}
	1181.7.26={
		liege=0
		holder=0 # Battle of Yokota-gawara
	}
	1183.9.5={
		liege=0
		holder=10120084 # Minamoto Yoshinaka {Echigo no Kami}
	}
	1183.9.11={
		holder=0 #  
	}
	1186.4.11={
		holder=10120075 # Minamoto Yoritomo
	}
	1190.3.9={
		holder=0 #  
	}
	1210.2.1={
		liege="e_nmih_japan"
		holder=10131653 # Hojo Yoshitoki
	}
	1223.10.1={
		holder=10131667 # Hojo Tomotoki
	}
	1224.2.1={
		holder=0 #  
	}
	1233.1.1={
		holder=10131696 # Hojo Mitsutoki
	}
	1246.6.1={
		holder=0 #  
	}
	1284.5.1={
		holder=10131840 # Hojo Kimitoki
	}
	1296.2.10={
		holder=10132180 # Hojo Tokiari
	}
	1333.5.1={
		holder=0 #  
	}
	1341.6.1={
		liege=0
		holder=10017006 # Uesugi Noriaki
	}
	1352.3.1={
		holder=0 #  
	}
	1362.1.1={
		holder=10017006 # Uesugi Noriaki
	}
	1368.10.31={
		holder=10032801 # Uesugi Noriyoshi
	}
	1386.1.1={
		holder=10017015 # Uesugi Fusakata
	}
	1421.12.4={
		holder=10032803 # Uesugi Tomokata
	}
	1422.10.29={
		holder=10032806 # Uesugi Fusatomo
	}
	1449.3.21={
		holder=10032808 # Uesugi Fusasada
	}
	1494.11.14={
		holder=10032811 # Uesugi Fusayoshi
	}
	1506.10.5={
		liege=0
		holder=10032000 # Nagao Tamekage
	}
	1543.1.29={
		holder=10032009 # Nagao Harukage
	}
	1548.12.30={
		holder=10032011 # Nagao Kagetora (Uesugi Kenshin)
	}
	1578.4.19={
		holder=10032032 # Uesugi Kagekatsu
	}

}
