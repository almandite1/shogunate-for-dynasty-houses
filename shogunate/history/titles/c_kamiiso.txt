c_nmih_kamiiso = {

	1.1.1={
		effect={
			title:b_nmih_kamiiso1={
				set_title_name=b_nmih_kamiiso2
			}
		}
	}
	1432.1.1={
		effect={
			title:b_nmih_kamiiso1={
				set_title_name=b_nmih_kamiiso2
			}
		}
	}

	1.1.1 = { change_development_level = 0 }
	100.1.1 = { change_development_level = 0 }
	900.1.1 = { change_development_level = 0 }
	1432.1.1 = { change_development_level = 20 }
	1457.1.1 = { change_development_level = 30 }
	1515.6.1 = { change_development_level = 20 }

	0100.1.1={
	}
	1170.1.1={
		liege=0
		holder=10150102 # Keironin
	}
	1210.1.1={
		liege=0
		holder=0 # 
	}
	1432.1.1={
		liege="k_nmih_wajinchi"
		holder=10000115 # Ando Yasusue
	}
	1457.1.1={
		liege="d_nmih_shimokuni"
		holder=10000119 # Ando Iemasa
	}
	1495.7.28={
		holder=10000121 # Ando Morosue
	}
	1512.5.1={
		liege="d_nmih_iburi"
		holder=10150502 #Shoya
	}
	1515.6.22={
		holder=10150504 #Cikomotayn
	}
	1560.1.1={
		holder=10150505 #Kuturayn
	}
	1575.3.15={
		holder=10150506 #Kaera
	}
}
