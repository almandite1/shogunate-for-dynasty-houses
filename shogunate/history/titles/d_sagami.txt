d_nmih_sagami = {
	0701.1.1={
		liege="e_nmih_tenno"
	}
	0800.1.1={
		holder=11005168 # Fujiwara Tomohito {Sagami no Kami}
	}
	0822.9.4={
		holder=0 #  
	}
	0849.2.1={
		holder=11000045 # Fujiwara Yoshimi {Sagami no Kami}
	}
	0850.7.4={
		holder=0 #  
	}
	0887.3.4={
		holder=11003302 # Fujiwara Toshimoto {Sagami no Kami}
	}
	0887.3.19={
		holder=0 #  
	}
	0969.12.6={
		holder=11005297 # Fujiwara Yasuchika {Sagami no Kami}
	}
	0973.1.1={
		holder=0 #  
	}
	1036.11.10={
		holder=10120035 # Minamoto Yoriyoshi {Sagami no Kami}
	}
	1040.1.1={
		holder=0 #  
	}
	1133.3.24={
		holder=11003590 # Fujiwara Takamori {Sagami no Kami}
	}
	1135.2.21={
		holder=0 #  
	}
	1152.3.13={
		holder=11005391 # Fujiwara Chikahiro {Sagami no Kami}
	}
	1180.1.1={
		liege = 0 # De facto
		holder=12215207 # Oba Kagechika
	}
	1180.10.26={
		liege=0
		holder=0
	}
	1186.4.11={
		liege="k_nmih_western_kanto"
		holder=10120075 # Minamoto Yoritomo
	}
	1363.1.1={
		holder=12211275 # Miura Takamichi
	}
	1376.1.1={
		holder=12211280 # Miura Takatsura
	}
	1410.1.1={
		holder=10016501 # Miura Takaaki
	}
	1426.1.1={
		holder=10049533 # Isshiki Tokiie
	}
	1439.3.24={
		holder=10017037 # Uesugi Mochitomo
	}
	1449.1.1={
		holder=10017038 # Uesugi Akifusa
	}
	1455.2.10={
		liege=0
		holder=10017037 # Uesugi Mochitomo
	}
	1467.10.4={
		holder=10017042 # Uesugi Masazane
	}
	1473.12.22={
		holder=10017040 # Uesugi Sadamasa
	}
	1494.11.20={
		holder=10017043 # Uesugi Tomoyoshi
	}
	1516.8.19={
		liege=0
		holder=10016001 # Hojo Soun
	}
	1519.9.8={
		holder=10016000 # Hojo Ujitsuna
	}
	1541.8.10={
		holder=10016004 # Hojo Ujiyasu
	}
	1546.5.19={
		liege="k_nmih_western_kanto"
	}
	1571.10.21={
		holder=10016014 # Hojo Ujimasa
	}

}
