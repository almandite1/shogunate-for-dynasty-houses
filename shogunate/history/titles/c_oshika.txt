c_nmih_oshika = {

	1.1.1={
		effect={
			title:b_nmih_oshika1={
				set_title_name=b_nmih_oshika6
			}
		}
	}
	1100.1.1={
		effect={
			title:b_nmih_oshika1={
				set_title_name=b_nmih_oshika9
			}
		}
	}

	1.1.1 = { change_development_level = 20 }
	1100.1.1 = { change_development_level = 20 }

	100.1.1={
	}
	1157.4.29={
		liege="k_nmih_northern_oshu"
		holder=12030020 # Fujiwara Hidehira
	}
	1187.11.30={
		holder=12030051 # Fujiwara Yasuhira
	}
	1189.9.21={
		liege="k_nmih_western_kanto" # Battle of Mountain Atsukashi
		holder=10120075 # Minamoto Yoritomo
	}
	1189.11.15={
		liege="d_nmih_rikuchu"
		holder=12201022 # Kasai Kiyoshige
	}
	1238.10.23={
		holder=10006140 # Kasai Tomokiyo
	}
	1250.1.1={
		holder=10006141 # Kasai Kiyochika
	}
	1270.1.1={
		holder=10006142 # Kasai Kiyotoki
	}
	1290.1.1={
		holder=10006143 # Kasai Kiyonobu
	}
	1310.1.1={
		holder=10006144 # Kasai Sadakiyo
	}
	1351.4.23={
		liege="d_nmih_rikuchu"
		holder=10006145 # Kasai Takakiyo
	}
	1365.5.8={
		holder=10006110 # Kasai Akikiyo
	}
	1388.1.1={
		holder=10006114 # Kasai Mitsunobu
	}
	1420.1.1={
		holder=10006116 # Kasai Mochinobu
	}
	1469.1.1={
		holder=10006120 # Kasai Tomonobu
	}
	1480.1.1={
		holder=10006122 # Kasai Hisanobu
	}
	1483.1.1={
		holder=10006104 # Kasai Masanobu
	}
	1506.5.23={
		holder=10006105 # Kasai Harushige
	}
	1534.1.5={
		holder=10006101 # Kasai Harutane
	}
	1555.1.1={
		holder=10006102 # Kasai Chikanobu
	}
	1560.6.5={
		holder=10006103 # Kasai Harunobu
	}

}
