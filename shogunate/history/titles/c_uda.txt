c_nmih_uda = {

	1.1.1={
		effect={
			title:b_nmih_uda1={
				set_title_name=b_nmih_uda1
			}
		}
	}
	1100.1.1={
		effect={
			title:b_nmih_uda1={
				set_title_name=b_nmih_uda9
			}
		}
	}
	1331.1.1={
		effect={
			title:b_nmih_uda1={
				set_title_name=b_nmih_uda1
			}
		}
	}

	1.1.1 = { change_development_level = 10 }
	1100.1.1 = { change_development_level = 20 }
	1331.1.1 = { change_development_level = 10 }

	0701.1.1={
		liege="d_nmih_yamato"
	}
	1179.5.20={
		liege="d_nmih_yamato" # Genen
		holder=12570101 # Genen
	}
	1181.1.12={
		liege="d_nmih_yamato" # Shinen
		holder=11000162 # Shinen. Siege of Nara
	}
	1189.7.13={
		liege=0
		holder=0
	}
	1383.7.1={
		liege="d_nmih_shima"
		holder=10026050 # Kitabatake Akiyasu
	}
	1414.1.1={
		holder=10026052 # Kitabatake Mitsumasa
	}
	1429.1.25={
		holder=10026006 # Kitabatake Noritomo
	}
	1471.4.13={
		holder=10026005 # Kitabatake Masasato
	}
	1508.12.15={
		holder=10026001 # Kitabatake Kichika
	}
	1517.1.1={
		holder=10026000 # Kitabatake Harutomo
	}
	1553.1.1={
		holder=10026003 # Kitabatake Tomonori
	}
	1560.1.1={ # Matsunaga Hisahide's invation of Yamato
		liege="d_nmih_yamato"
		holder=10045720 # Takayama Tomoteru
	}
	1568.10.20={
		holder=10045400 # Matsunaga Hisahide
	}
	1573.12.10={ # Matsunaga surrendered to Oda Nobunaga
		holder=10029120 # Oda Nobunaga
	}
	1575.5.15={
		holder=10029390 # Ban Naomasa
	}
	1576.5.30={
		holder=10029120 # Oda Nobunaga
	}
	1576.6.6={
		holder=10048413 # Tsutsui Junkei
	}

}
