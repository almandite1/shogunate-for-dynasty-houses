c_nmih_horobetsu = {

	1.1.1={
		effect={
			title:b_nmih_horobetsu1={
				set_title_name=b_nmih_horobetsu1
			}
		}
	}

	1.1.1 = { change_development_level = 0 }
	100.1.1 = { change_development_level = 0 }
	900.1.1 = { change_development_level = 0 }
	1600.1.1 = { change_development_level = 10 }
	1799.1.1 = { change_development_level = 10 }

	0100.1.1={
	}
	1066.1.1={
		holder=10150200 # Okikirmuy
	}
	1130.1.1={
		liege=0
		holder=0 # 
	}
	1170.1.1={
		liege="d_nmih_hidaka"
		holder=10151100 # Kirumain
	}
	1210.1.1={
		liege=0
		holder=0 # 
	}
	1440.1.1={
		liege=d_nmih_hidaka
		holder=10151099
	}
	1460.1.1={
		holder=10151098
	}
	1490.1.1={
		holder=10151000
	}
	1500.1.1={
		holder=10151001
	}
	1540.1.1={
		holder=10151002
	}
	1560.1.1={
		holder=10151003
	}
	1590.1.1={
		holder=10151004
	}

}
