d_nmih_hoki = {
	0701.1.1={
		liege="e_nmih_tenno"
	}
	0878.9.18={
		holder=11004544 # Fujiwara no Yasutsugu
	}
	0886.2.27={
		holder=0 #  
	}
	0993.1.1={
		holder=10220040 # Oe no Kiyomichi
	}
	0997.7.9={
		holder=0
	}
	1016.1.24={
		holder=11003338 # Fujiwara no Takasuke
	}
	1021.1.1={
		holder=0
	}
	1091.1.1={
		holder=11006695 # Fujiwara no Takatada
	}
	1094.2.22={
		holder=0
	}
	1117.11.26={
		holder=10110026 # Taira no Tadamori
	}
	1120.11.25={
		holder=0
	}
	1180.1.1={
		liege=0
	}
	1264.10.9={
		holder=10131997 # Hojo Tokisuke
	}
	1272.3.15={
		holder=0 #  
	}
	1330.1.1={
		holder=10132083 # Hojo Tokimasu
	}
	1333.6.21={
		holder=0 #  
	}
	1336.1.1={
		holder=0 #  
	}
	1337.3.1={
		holder=12186256 # Yamana Tokiuji
	}
	1351.8.1={
		holder=0 #  
	}
	1363.9.1={
		holder=12186256 # Yamana Tokiuji
	}
	1371.2.1={
		holder=10050011 # Yamana Tokiyoshi
	}
	1389.5.29={
		holder=10050013 # Yamana Ujiyuki
	}
	1390.1.1={
		holder=10050014 # Yamana Mitsuyuki
	}
	1391.12.1={
		holder=10050013 # Yamana Ujiyuki
	}
	1424.11.1={
		holder=10050017 # Yamana Noriyuki
	}
	1453.1.1={
		holder=10050018 # Yamana Toyoyuki
	}
	1467.6.27={
		liege="k_nmih_sanin"
	}
	1471.10.31={
		holder=10050023 # Yamana Masayuki
	}
	1473.4.15={
		liege=0
	}
	1476.1.1={
		holder=10050021 # Yamana Motoyuki
	}
	1489.1.1={
		holder=10050023 # Yamana Masayuki
	}
	1491.1.1={
		holder=10050024 # Yamana Hisayuki
	}
	1494.9.24={
		holder=10050026 # Yamana Sumiyuki
	}
	1524.1.1={
		holder=0 # 
	}
}
