c_nmih_nei = {

	1.1.1={
		effect={
			title:b_nmih_nei1={
				set_title_name=b_nmih_nei3
			}
		}
	}
	1100.1.1={
		effect={
			title:b_nmih_nei1={
				set_title_name=b_nmih_nei9
			}
		}
	}
	1331.1.1={
		effect={
			title:b_nmih_nei1={
				set_title_name=b_nmih_nei1
			}
		}
	}
	1440.1.1={
		effect={
			title:b_nmih_nei1={
				set_title_name=B_NMIH_NEI1_1440
			}
		}
	}

	1.1.1 = { change_development_level = 20 }
	1100.1.1 = { change_development_level = 20 }
	1331.1.1 = { change_development_level = 30 }

	0702.10.14={
		liege="d_nmih_etchu"
	}
	1179.12.18={
		liege="d_nmih_etchu"
		holder=10110142 # Taira Nariie
	}
	1183.6.22={
		liege="k_nmih_hokuriku"
		holder=10120084 # Minamoto Yoshinaka / Battle of Shinohara
	}
	1183.8.17={
		liege="e_nmih_japan"
	}
	1184.3.4={
		liege=0
		holder=0 # Battle of Awazu
	}
	1380.7.1={
		liege="d_nmih_etchu"
		holder=10031205 # Jinbo Kunihisa
	}
	1420.1.1={
		holder=10031206 # Jinbo Norihisa
	}
	1440.1.1={
		holder=10031207 # Jinbo Kunimune
	}
	1454.4.1={
		holder=10031201 # Jinbo Naganobu
	}
	1501.12.18={
		holder=10031202 # Jinbo Yoshimune
	}
	1519.1.1={
		liege=0
	}
	1521.1.30={
		holder=10031200 # Jinbo Nagamoto
	}
	1572.1.1={
		holder=10031204 # Jinbo Naganari
	}
	1576.1.1={
		liege="d_nmih_echigo"
		holder=10032011 # Uesugi Kenshin
	}
	1576.1.2={
		liege="d_nmih_echigo"
		holder=10032641 # Yosie Munenobu
	}
	1578.6.1={
		liege="e_nmih_japan" # 29120 Oda Nobunaga
		holder=10031203 # Jinbo Nagazumi
	}
	1581.03.05={
		liege="d_nmih_etchu" # 29350 Sassa Narimasa
	}
	1582.4.1={
		liege="d_nmih_etchu" # 29350 Sassa Narimasa
		holder=10029350 # Sassa Narimasa
	}
}
