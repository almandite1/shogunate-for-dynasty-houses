c_nmih_muko = {

	1.1.1={
		effect={
			title:b_nmih_muko1={
				set_title_name=b_nmih_muko1
			}
		}
	}
	1331.1.1={
		effect={
			title:b_nmih_muko1={
				set_title_name=b_nmih_muko5
			}
		}
	}
	1516.1.1={
		effect={
			title:b_nmih_muko1={
				set_title_name=b_nmih_muko4
			}
		}
	}

	1.1.1 = { change_development_level = 10 }
	1331.1.1 = { change_development_level = 30 }
	1516.1.1 = { change_development_level = 30 }

	0701.1.1={
		liege="d_nmih_settsu"
	}
	1179.12.18={
		liege="e_nmih_japan" # Taira Kiyomori
		holder=10110025 # Taira Kiyomori
	}
	1181.3.20={
		liege="e_nmih_japan" # Taira Munemori
		holder=10110042 # Taira Munemori
	}
	1183.8.17={
		liege="e_nmih_japan"
		holder=10120084 # Minamoto Yoshinaka / Taira's escape to the west
	}
	1184.3.4={
		liege=0
		holder=0 # Battle of Awazu
	}
	1397.6.2={
		liege="d_nmih_settsu"
		holder=10040536 # Hosokawa Mitsumoto
	}
	1426.11.15={
		holder=10040541 # Hosokawa Mochimoto
	}
	1429.8.14={
		holder=10040539 # Hosokawa Mochiyuki
	}
	1442.9.8={
		holder=10040543 # Hosokawa Katsumoto
	}
	1467.1.1={
		holder=10040850 # Yakushiji Motonaga
	}
	1502.1.25={
		holder=10040852 # Yakushiji Nagatada
	}
	1507.9.7={
		holder=10040854 # Yakushiji Kunimori
	}
	1508.1.1={
		liege="k_nmih_northern_kinai"
	}
	1508.7.28={
		liege="d_nmih_settsu"
	}
	1508.9.2={
		liege="k_nmih_southern_kinai"
		holder=10040501 # Hosokawa Sumimoto
	}
	1508.9.16={
		liege="d_nmih_settsu"
		holder=10040854 # Yakushiji Kunimori
	}
	1520.2.21={
		liege="k_nmih_southern_kinai"
		holder=10059001 # Miyoshi Yukinaga
	}
	1520.6.6={
		liege="d_nmih_settsu"
		holder=10040854 # Yakushiji Kunimori
	}
	1527.3.14={
		liege="k_nmih_kanrei"
		holder=10059003 # Miyoshi Motonaga
	}
	1532.7.22={
		holder=10059007 # Miyoshi Nagayoshi
	}
	1548.11.28={
		liege="k_nmih_southern_kinai"
	}
	1553.10.1={
		liege="k_nmih_southern_kinai"
		holder=10045400 # Matsunaga Hisahide
	}
	1560.1.1={
		liege="d_nmih_yamato"
	}
	1566.7.30={
		liege="d_nmih_awa"
		holder=10045451 # Shinonara Nagafusa
	}
	1568.10.22={
		liege="k_nmih_chubu"
		holder=10045800 # Wada Koremasa
	}
	1571.10.1={
		liege="d_nmih_awa"
		holder=10045451 # Shinonara Nagafusa
	}
	1573.8.13={
		liege="d_nmih_settsu"
		holder=10045700 # Araki Murashige
	}
	1580.8.12={
		liege="e_nmih_japan"
		holder=10029120 # Oda Nobunaga
	}
	1580.9.1={
		holder=10029552 # Ikeda Tsuneoki
	}
	1582.6.21={
		liege="d_nmih_harima"
	}
	1582.7.16={
		liege="d_nmih_settsu"
	}

}
