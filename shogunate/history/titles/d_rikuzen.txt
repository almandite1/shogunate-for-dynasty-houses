d_nmih_rikuzen = {
	100.1.1={
	}
	1157.4.29={
		liege="k_nmih_northern_oshu"
		holder=12030020 # Fujiwara Hidehira
	}
	1187.11.30={
		holder=12030051 # Fujiwara Yasuhira
	}
	1189.9.21={
		liege=0
		holder=0 # 
	}
	1354.1.1={
		liege="k_nmih_oshu_tandai"
		holder=12139050 # Osaki Iekane
	}
	1356.7.11={
		holder=10006217 # Osaki Tadamochi
	}
	1383.1.1={
		holder=10006213 # Osaki Akimochi
	}
	1400.1.1={
		holder=10006212 # Osaki Mitsuakira
	}
	1404.1.1={
		holder=10006210 # Osaki Mitsumochi
	}
	1424.1.1={
		holder=10006208 # Osaki Mochiakira
	}
	1441.1.1={
		holder=10006207 # Osaki Norikane
	}
	1484.1.1={
		holder=10006206 # Osaki Masakane
	}
	1487.9.22={
		holder=10006203 # Osaki Yoshikane
	}
	1529.1.1={
		holder=10006204 # Osaki Takakane
	}
	1530.1.1={
		holder=10006201 # Osaki Yoshinao
	}
	1555.1.1={
		liege=0
		holder=10006201 # Osaki Yoshinao
	}
	1577.1.1={
		holder=10006219 # Osaki Yoshitaka
	}
}
