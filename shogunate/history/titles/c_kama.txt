c_nmih_kama = {

	1.1.1={
		effect={
			title:b_nmih_kama1={
				set_title_name=b_nmih_kama2
			}
		}
	}
	1400.1.1={
		effect={
			title:b_nmih_kama1={
				set_title_name=b_nmih_kama8
			}
		}
	}

	1.1.1 = { change_development_level = 10 }
	1400.1.1 = { change_development_level = 10 }

	100.1.1={
	}
	180.1.1={
		holder=10290001 # Himiko
	}
	1180.1.1={
		liege="d_nmih_buzen"
		holder=12780700 # Yamaga Hideto
	}
	1185.3.4={
		liege="k_nmih_western_chugoku" # Battle of Ashiya-no-ura
	}
	1185.4.25={
		liege=0
		holder=0 # Battle of Dan-no-ura
	}
	1429.1.1={
		liege="k_nmih_western_chugoku"
		holder=10057010 # Ouchi Moriharu
	}
	1431.8.6={
		holder=10057015 # Ouchi Mochiyo
	}
	1441.8.14={
		holder=10057008 # Ouchi Norihiro
	}
	1465.9.23={
		holder=10057007 # Ouchi Masahiro
	}
	1478.1.1={
		holder=10057101 # Sue Hiromori
	}
	1479.1.1={
		holder=10057111 # Sue Hiroaki
	}
	1517.1.1={
		holder=10067201 # Sugi Okinaga
	}
	1533.1.1={
		holder=10067200 # Sugi Okikazu
	}
	1551.9.30={
		holder=10057102 # Sue Harukata
	}
	1555.10.16={
		holder=10057116 # Sue Nagafusa
	}
	1557.4.1={
		liege="d_nmih_aki"
		holder=10056004 # Mori Motonari
	}
	1557.5.1={
		liege="d_nmih_bungo"
		holder=10064002 # Otomo Yoshishige
	}
	1559.1.1={
		liege="k_nmih_western_chugoku"
		holder=10067505 # Akizuki Tanesane
	}
	1564.8.1={
		liege="d_nmih_bungo"
	}
	1567.1.1={
		liege="d_nmih_chikugo"
	}
	1569.8.1={
		liege="d_nmih_chikuzen"
	}
	1578.12.10={
		liege="d_nmih_chikuzen"
	}

}
