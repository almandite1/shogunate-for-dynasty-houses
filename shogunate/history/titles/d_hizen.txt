d_nmih_hizen = {
	0701.1.1={
		liege="e_nmih_tenno"
	}
	764.11.6={
		holder=10041023 # Empress Koken/Shotoku
	}
	1180.1.1={
		liege=0
		holder=10240113 # Takagi Muneie
	}
	1190.1.1={
		holder=0 #
	}
	1195.1.1={
		holder=10130546 # Muto Sukeyori
	}
	1228.9.24={
		holder=0
	}
	1230.1.1={
		holder=10130550 # Muto Sukeyoshi
	}
	1273.1.1={
		holder=0
	}
	1276.1.1={
		holder=10068029 # Shoni Tsunesuke
	}
	1281.1.1={
		holder=10131825 # Hojo Tokisada
	}
	1293.7.24={
		holder=10132162 # Hojo Kanetoki
	}
	1296.7.1={
		holder=10131932 # Hojo Sanemasa
	}
	1301.9.1={
		holder=10132097 # Hojo Masaaki
	}
	1317.2.1={
		holder=10132256 # Hojo Yukitoki
	}
	1321.12.25={
		holder=10132200 # Hojo Hidetoki
	}
	1333.7.1={
		holder=0
	}
	1334.1.9={
		holder=10064013 # Otomo Ujiyasu
	}
	1359.1.1={
		holder=10068018 # Shoni Yorihisa
	}
	1361.1.1={
		holder=10066022 # Kikuchi Takemitsu
	}
	1372.1.1={
		holder=12395221 # Imagawa Sadayo
	}
	1396.1.1={
		liege="k_nmih_kyushu_tandai"
		holder=10057612 # Shibukawa Mitsuyori
	}
	1419.1.1={
		holder=10057618 # Shibukawa Yoshitoshi
	}
	1428.1.1={
		holder=10057614 # Shibukawa Mitsunao
	}
	1434.1.1={
		holder=10057615 # Shibukawa Norinao
	}
	1479.1.1={
		holder=10057623 # Shibukawa Manjumaru
	}
	1487.1.1={
		holder=10057601 # Shibukawa Tadashige
	}
	1504.1.1={
		liege=0
		holder=10068000 # Shoni Sukemoto
	}
	1536.9.19={
		holder=10068003 # Shoni Fuyuhisa
	}
	1559.2.18={
		liege=0
		holder=10068320 # Ryuzoji Takanobu
	}
	1584.5.4={
		liege="k_nmih_southern_kyushu"
		holder=10068331 # Ryuzoji Masaie
	}

}
