c_nmih_kasuya = {

	1.1.1={
		effect={
			title:b_nmih_kasuya1={
				set_title_name=b_nmih_kasuya1
			}
		}
	}
	1180.1.1={
		effect={
			title:b_nmih_kasuya1={
				set_title_name=B_NMIH_KASUYA1_1180
			}
		}
	}
	1330.1.1={
		effect={
			title:b_nmih_kasuya1={
				set_title_name=B_NMIH_KASUYA1_1330
			}
		}
	}

	1.1.1 = { change_development_level = 20 }

	100.1.1={
	}
	240.1.1={
		holder=10290040 # Tamo
		liege=d_nmih_chikuzen
	}
	1180.1.1={
		liege="d_nmih_chikuzen"
		holder=12750100 # Harada Taneto
	}
	1185.3.4={
		liege=0
		holder=0 # Battle of Ashiya-no-ura
	}
	1387.1.1={
		liege="d_nmih_chikuzen"
		holder=10068013 # Shoni Sadayori
	}
	1404.7.27={
		holder=10068005 # Shoni Mitsusada
	}
	1433.9.29={
		holder=10068012 # Shoni Yoshiyori
	}
	1441.1.31={
		holder=10068004 # Shoni Noriyori
	}
	1469.1.18={
		holder=10068001 # Shoni Masasuke
	}
	1497.5.21={
		liege="d_nmih_bungo"
		holder=10067401 # Tachibana Munekatsu
	}
	1527.1.1={
		holder=10067400 # Tachibana Akimitsu
	}
	1557.8.6={
		holder=10067402 # Tachibana Akitoshi
	}
	1567.1.1={
		liege="d_nmih_chikugo"
		holder=10067402 # Tachibana Akitoshi
	}
	1568.9.5={
		liege="d_nmih_bungo"
		holder=10067406 # Tachibana Dosetsu(Bekki Akitsura)
	}
	1569.12.1={
		liege="d_nmih_chikugo"
	}
	1585.11.2={
		liege="d_nmih_chikugo"
		holder=10067408 # Tachibana Muneshige(Takahashi Munetora)
	}

}
