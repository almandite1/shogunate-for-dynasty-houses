c_nmih_kanzaki = {

	1.1.1={
		effect={
			title:b_nmih_kanzaki1={
				set_title_name=b_nmih_kanzaki2
			}
		}
	}
	1100.1.1={
		effect={
			title:b_nmih_kanzaki1={
				set_title_name=b_nmih_kanzaki9
			}
		}
	}
	1331.1.1={
		effect={
			title:b_nmih_kanzaki1={
				set_title_name=b_nmih_kanzaki4
			}
		}
	}
	1467.1.1={
		effect={
			title:b_nmih_kanzaki1={
				set_title_name=b_nmih_kanzaki1
			}
		}
	}

	1.1.1 = { change_development_level = 20 }
	1100.1.1 = { change_development_level = 20 }
	1331.1.1 = { change_development_level = 20 }
	1467.1.1 = { change_development_level = 20 }

	100.1.1={
	}
	1179.12.18={
		liege = "d_nmih_harima"
		holder=10110070 # Taira Yukimori
	}
	1183.8.17={
		liege=0
		holder=0 # Taira's escape to the west
	}
	1336.1.1={
		liege="d_nmih_harima"
		holder=10053500 # Akamatsu Norimura
	}
	1350.2.18={
		holder=10053501 # Akamatsu Norisuke
	}
	1351.5.4={
		holder=10053503 # Akamatsu Norisuke
	}
	1372.1.13={
		holder=10053507 # Akamatsu Yoshinori
	}
	1427.10.11={
		holder=10053511 # Akamatsu  Mitsusuke
	}
	1441.9.25={
		liege="d_nmih_harima"
		holder=10050039 # Yamana Mochitoyo
	}
	1454.1.1={
		holder=10050040 # Yamana Noritoyo
	}
	1458.1.1={
		holder=10050039 # Yamana Mochitoyo
	}
	1467.6.27={
		liege="d_nmih_harima"
		holder=10053518 # Akamatsu Masanori
	}
	1496.6.6={
		holder=10053519 # Akamatsu Yoshimura
	}
	1520.11.1={
		holder=10053502 # Akamatsu Harumasa
	}
	1558.8.1={
		holder=10053533 # Akamatsu Yoshisuke
	}
	1570.1.1={
		holder=10053536 # Akamatsu Norifusa
	}

}
