c_nmih_tetta = {

	1.1.1={
		effect={
			title:b_nmih_tetta1={
				set_title_name=b_nmih_tetta3
			}
		}
	}
	1100.1.1={
		effect={
			title:b_nmih_tetta1={
				set_title_name=b_nmih_tetta3
			}
		}
	}
	1331.1.1={
		effect={
			title:b_nmih_tetta1={
				set_title_name=b_nmih_tetta1
			}
		}
	}

	1.1.1 = { change_development_level = 10 }
	1100.1.1 = { change_development_level = 10 }
	1331.1.1 = { change_development_level = 20 }

	100.1.1={
	}
	1180.1.1={
		liege = "d_nmih_bitchu"
		holder=12630500 # Tajibe Motoharu
	}
	1220.1.1={
		liege=0
		holder=0 # 
	}
	1222.1.1={
		holder = 12631000 # Niimi Sukemitsu
	}
	1440.1.1={
		holder = 12631010 # Niimi Kiyonao
	}
	1480.1.1={
		holder = 12631012 # Niimi Tsunenao
	}
	1500.1.1={
		holder = 12631013 # Niimi Katanao
	}
	1510.1.1={
		holder = 12631017 # Niimi Kunitsune
	}
	1517.1.1={
		liege="k_nmih_sanin"
	}
	1542.1.1={
		holder = 12631018 # Niimi Sadatsune
	}
	1566.1.1={
		liege="d_nmih_bitchu"
		holder=10054907 # Mimura Motonori
	}
	1575.7.9={
		liege="d_nmih_izumo"
		holder=10056015 # Kikkawa Motoharu
	}

}
