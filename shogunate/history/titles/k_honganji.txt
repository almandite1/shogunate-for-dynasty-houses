k_nmih_honganji = {
	1000.1.1={
		government = theocracy_government
	}
	1263.1.16 = {
		holder=11003985 # Honganji Shinran
	}
	1272.1.1={
		holder=10042284 # Honganji Nyoshin
	}
	1312.1.11={
		holder=10042226 # Honganji Kakunyo
	}
	1351.2.15={
		holder=10042223 # Honganji Zennyo
	}
	1389.3.27={
		holder=10042222 # Honganji Shakunyo
	}
	1393.6.4={
		holder=10042221 # Honganji Gyonyo
	}
	1440.11.8={
		holder=10042220 # Honganji Zonnyo
	}
	1453.7.9={
		holder=10042201 # Honganji Rennyo
	}
	1489.1.1={
		holder=10042203 # Honganji Jitsunyo
	}
	1525.3.5={
		holder=10042200 # Honganji Shonyo
	}
	1554.9.9={
		holder=10042210 # Honganji Kennyo
	}
	1592.12.27={
		holder=10042211 # Honganji Kyonyo
	}
	1593.11.9={
		holder=10042212 # Honganji Junnyo
	}

}
