c_nmih_kumano = {

	1.1.1={
		effect={
			title:b_nmih_kumano1={
				set_title_name=b_nmih_kumano1
			}
		}
	}
	1331.1.1={
		effect={
			title:b_nmih_kumano1={
				set_title_name=b_nmih_kumano3
			}
		}
	}

	1.1.1 = { change_development_level = 10 }
	1331.1.1 = { change_development_level = 10 }

	0701.1.1={
		liege="d_nmih_kii"
	}
	1180.1.1={
		liege=0
		holder=12550011 # Kumano Hanchi
	}
	1184.10.1={
		holder=12550030 # Kumano Tanzo
	}
	1198.6.14={
		liege=0
		holder=0 # 
	}
	1450.1.1={
		liege=0
		holder=10048503 # Horiuchi Ujitada
	}
	1467.6.27={ # Onin War
		liege="d_nmih_kawachi" # Onin War
	}
	1477.12.16={ # End of Onin war
		liege = 0
	}
	1480.1.1={
		holder=10048502 # Horiuchi Ujisada
	}
	1510.1.1={
		holder=10048502 # Horiuchi Ujimitsu
	}
	1530.1.1={
		holder=10048500 # Horiuchi Ujitora
	}
	1574.1.1={
		holder=10048505 # Horiuchi Ujiyoshi
	}
	1581.1.1={
		liege="e_nmih_japan" # 29120 Oda Nobunaga
		holder=10048505 # Horiuchi Ujiyoshi
	}
	1582.6.21={
		liege=0
		holder=10048505 # Horiuchi Ujiyoshi
	}
}
