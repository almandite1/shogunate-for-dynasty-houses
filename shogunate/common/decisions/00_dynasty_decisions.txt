﻿create_cadet_branch_decision = {
	picture = "gfx/interface/illustrations/decisions/decision_dynasty_house.dds"

	major = yes
	ai_check_interval = 0

	is_shown = {
		is_landed = yes
		exists = house
		exists = house.house_head
		is_house_head = no
		primary_title.tier > tier_barony
		NOR = {
			has_government = holy_order_government
			has_government = theocracy_government
			AND = {
				faith = { has_doctrine = doctrine_theocracy_temporal }
				has_council_position = councillor_court_chaplain
			}
		}
	}

	is_valid = {
		house.house_head = {
			government_allows = create_cadet_branches
			custom_description = {
				text = create_cadet_branch_decision_succession_line
				NOT = {
					any_held_title = {
						place_in_line_of_succession = {
							target = root
							value <=3
						}
					}
				}
			}
		}

		custom_description = {
			text = create_cadet_branch_decision_ancestor_in_house
			NOT = {
				any_ancestor = {
					house = root.house
				}
			}
		}
		
		trigger_if = {
			limit = {
				is_married = yes
				is_male = yes
			}
			patrilinear_marriage = yes
		}
		trigger_if = {
			limit = {
				is_married = yes
				is_female = yes
			}
			matrilinear_marriage = yes
		}
		trigger_if = { #Males of female-dominated faith must already be patrilineally married before taking this decision.
			limit = {
				is_married = no
				is_female = no
				faith = {
					has_doctrine = doctrine_gender_female_dominated
				}
			}
			patrilinear_marriage = yes
		}
		trigger_if = { #Females of male-dominated faith must already be matrilineally married before taking this decision.
			limit = {
				is_married = no
				is_female = yes
				faith = {
					has_doctrine = doctrine_gender_male_dominated
				}
			}
			matrilinear_marriage = yes
		}
		trigger_if = {
			limit = {
				has_trait = devoted
			}
			NOT = {
				has_trait = devoted
			}
		}
		trigger_if = {
			limit = {
				has_trait = bastard
			}
			NOT = {
				has_trait = bastard
			}
		}
	}

	is_valid_showing_failures_only = {
		is_available_adult = yes
		is_landed = yes
	}

	effect = {
		found_cadet_house_decision_effect = {
			CHARACTER = root
			PRESTIGE = major_prestige_gain
		}
		house = {
			generate_coa = yes
		}
		title_to_kamon_coa_effect = yes
	}
}
