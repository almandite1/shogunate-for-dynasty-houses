# Auto generated file, do not edit manually. Created using console command dump_bookmark_portraits
# History database id:10120075
bm_genpeiwar1180_minamoto_yoritomo={
	type=male
	id=6367
	age=0.330000
	genes={ 		hair_color={ 109 248 125 238 }
 		skin_color={ 167 101 172 102 }
 		eye_color={ 61 248 79 245 }
 		gene_chin_forward={ "chin_forward_pos" 146 "chin_forward_neg" 112 }
 		gene_chin_height={ "chin_height_neg" 126 "chin_height_neg" 117 }
 		gene_chin_width={ "chin_width_pos" 145 "chin_width_neg" 122 }
 		gene_eye_angle={ "eye_angle_neg" 114 "eye_angle_neg" 118 }
 		gene_eye_depth={ "eye_depth_pos" 154 "eye_depth_pos" 191 }
 		gene_eye_height={ "eye_height_neg" 122 "eye_height_neg" 121 }
 		gene_eye_distance={ "eye_distance_neg" 127 "eye_distance_neg" 114 }
 		gene_eye_shut={ "eye_shut_pos" 221 "eye_shut_neg" 102 }
 		gene_forehead_angle={ "forehead_angle_pos" 155 "forehead_angle_pos" 134 }
 		gene_forehead_brow_height={ "forehead_brow_height_pos" 144 "forehead_brow_height_neg" 117 }
 		gene_forehead_roundness={ "forehead_roundness_pos" 141 "forehead_roundness_neg" 105 }
 		gene_forehead_width={ "forehead_width_pos" 141 "forehead_width_neg" 123 }
 		gene_forehead_height={ "forehead_height_pos" 136 "forehead_height_pos" 170 }
 		gene_head_height={ "head_height_pos" 132 "head_height_pos" 141 }
 		gene_head_width={ "head_width_neg" 113 "head_width_neg" 93 }
 		gene_head_profile={ "head_profile_neg" 117 "head_profile_pos" 144 }
 		gene_head_top_height={ "head_top_height_neg" 120 "head_top_height_neg" 104 }
 		gene_head_top_width={ "head_top_width_neg" 112 "head_top_width_pos" 142 }
 		gene_jaw_angle={ "jaw_angle_pos" 160 "jaw_angle_neg" 112 }
 		gene_jaw_forward={ "jaw_forward_neg" 115 "jaw_forward_neg" 109 }
 		gene_jaw_height={ "jaw_height_neg" 115 "jaw_height_pos" 127 }
 		gene_jaw_width={ "jaw_width_pos" 144 "jaw_width_neg" 104 }
 		gene_mouth_corner_depth={ "mouth_corner_depth_pos" 133 "mouth_corner_depth_neg" 103 }
 		gene_mouth_corner_height={ "mouth_corner_height_pos" 131 "mouth_corner_height_pos" 134 }
 		gene_mouth_forward={ "mouth_forward_neg" 110 "mouth_forward_neg" 108 }
 		gene_mouth_height={ "mouth_height_pos" 151 "mouth_height_neg" 125 }
 		gene_mouth_width={ "mouth_width_neg" 117 "mouth_width_pos" 129 }
 		gene_mouth_upper_lip_size={ "mouth_upper_lip_size_neg" 111 "mouth_upper_lip_size_pos" 131 }
 		gene_mouth_lower_lip_size={ "mouth_lower_lip_size_neg" 86 "mouth_lower_lip_size_neg" 108 }
 		gene_mouth_open={ "mouth_open_neg" 119 "mouth_open_neg" 29 }
 		gene_neck_length={ "neck_length_neg" 92 "neck_length_neg" 125 }
 		gene_neck_width={ "neck_width_neg" 116 "neck_width_neg" 106 }
 		gene_bs_cheek_forward={ "cheek_forward_neg" 14 "cheek_forward_neg" 42 }
 		gene_bs_cheek_height={ "cheek_height_pos" 20 "cheek_height_neg" 114 }
 		gene_bs_cheek_width={ "cheek_width_pos" 12 "cheek_width_neg" 90 }
 		gene_bs_ear_angle={ "ear_angle_pos" 23 "ear_angle_pos" 28 }
 		gene_bs_ear_inner_shape={ "ear_inner_shape_pos" 88 "ear_inner_shape_pos" 24 }
 		gene_bs_ear_bend={ "ear_upper_bend_pos" 39 "ear_upper_bend_pos" 40 }
 		gene_bs_ear_outward={ "ear_outward_neg" 41 "ear_outward_neg" 22 }
 		gene_bs_ear_size={ "ear_size_neg" 66 "ear_size_neg" 117 }
 		gene_bs_eye_corner_depth={ "eye_corner_depth_neg" 104 "eye_corner_depth_neg" 51 }
 		gene_bs_eye_fold_shape={ "eye_fold_shape_neg" 98 "eye_fold_shape_neg" 39 }
 		gene_bs_eye_size={ "eye_size_neg" 177 "eye_size_neg" 165 }
 		gene_bs_eye_upper_lid_size={ "eye_upper_lid_size_neg" 2 "eye_upper_lid_size_neg" 38 }
 		gene_bs_forehead_brow_curve={ "forehead_brow_curve_pos" 50 "forehead_brow_curve_neg" 3 }
 		gene_bs_forehead_brow_forward={ "forehead_brow_forward_neg" 160 "forehead_brow_forward_neg" 57 }
 		gene_bs_forehead_brow_inner_height={ "forehead_brow_inner_height_neg" 41 "forehead_brow_inner_height_pos" 14 }
 		gene_bs_forehead_brow_outer_height={ "forehead_brow_outer_height_pos" 24 "forehead_brow_outer_height_pos" 64 }
 		gene_bs_forehead_brow_width={ "forehead_brow_width_neg" 52 "forehead_brow_width_neg" 34 }
 		gene_bs_jaw_def={ "jaw_def_neg" 75 "jaw_def_pos" 127 }
 		gene_bs_mouth_lower_lip_def={ "mouth_lower_lip_def_pos" 8 "mouth_lower_lip_def_pos" 7 }
 		gene_bs_mouth_lower_lip_full={ "mouth_lower_lip_full_neg" 27 "mouth_lower_lip_full_neg" 62 }
 		gene_bs_mouth_lower_lip_pad={ "mouth_lower_lip_pad_neg" 4 "mouth_lower_lip_pad_pos" 28 }
 		gene_bs_mouth_lower_lip_width={ "mouth_lower_lip_width_pos" 37 "mouth_lower_lip_width_neg" 65 }
 		gene_bs_mouth_philtrum_def={ "mouth_philtrum_def_pos" 43 "mouth_philtrum_def_pos" 30 }
 		gene_bs_mouth_philtrum_shape={ "mouth_philtrum_shape_neg" 38 "mouth_philtrum_shape_neg" 106 }
 		gene_bs_mouth_philtrum_width={ "mouth_philtrum_width_pos" 0 "mouth_philtrum_width_pos" 22 }
 		gene_bs_mouth_upper_lip_def={ "mouth_upper_lip_def_pos" 45 "mouth_upper_lip_def_pos" 99 }
 		gene_bs_mouth_upper_lip_full={ "mouth_upper_lip_full_pos" 5 "mouth_upper_lip_full_pos" 1 }
 		gene_bs_mouth_upper_lip_profile={ "mouth_upper_lip_profile_neg" 18 "mouth_upper_lip_profile_pos" 55 }
 		gene_bs_mouth_upper_lip_width={ "mouth_upper_lip_width_neg" 88 "mouth_upper_lip_width_neg" 61 }
 		gene_bs_nose_forward={ "nose_forward_neg" 27 "nose_forward_neg" 46 }
 		gene_bs_nose_height={ "nose_height_neg" 95 "nose_height_neg" 45 }
 		gene_bs_nose_length={ "nose_length_neg" 17 "nose_length_neg" 13 }
 		gene_bs_nose_nostril_height={ "nose_nostril_height_neg" 0 "nose_nostril_height_neg" 20 }
 		gene_bs_nose_nostril_width={ "nose_nostril_width_neg" 72 "nose_nostril_width_neg" 7 }
 		gene_bs_nose_profile={ "nose_profile_neg" 112 "nose_profile_neg" 110 }
 		gene_bs_nose_ridge_angle={ "nose_ridge_angle_neg" 17 "nose_ridge_angle_pos" 63 }
 		gene_bs_nose_ridge_width={ "nose_ridge_width_neg" 6 "nose_ridge_width_pos" 36 }
 		gene_bs_nose_size={ "nose_size_pos" 45 "nose_size_neg" 153 }
 		gene_bs_nose_tip_angle={ "nose_tip_angle_pos" 44 "nose_tip_angle_neg" 10 }
 		gene_bs_nose_tip_forward={ "nose_tip_forward_neg" 154 "nose_tip_forward_neg" 103 }
 		gene_bs_nose_tip_width={ "nose_tip_width_pos" 56 "nose_tip_width_pos" 68 }
 		face_detail_cheek_def={ "cheek_def_01" 106 "cheek_def_02" 124 }
 		face_detail_cheek_fat={ "cheek_fat_01_neg" 52 "cheek_fat_04_pos" 172 }
 		face_detail_chin_cleft={ "chin_dimple" 13 "chin_dimple" 12 }
 		face_detail_chin_def={ "chin_def" 0 "chin_def" 30 }
 		face_detail_eye_lower_lid_def={ "eye_lower_lid_def" 185 "eye_lower_lid_def" 39 }
 		face_detail_eye_socket={ "eye_socket_03" 62 "eye_socket_02" 60 }
 		face_detail_nasolabial={ "nasolabial_02" 16 "nasolabial_01" 17 }
 		face_detail_nose_ridge_def={ "nose_ridge_def_pos" 164 "nose_ridge_def_neg" 13 }
 		face_detail_nose_tip_def={ "nose_tip_def" 32 "nose_tip_def" 237 }
 		face_detail_temple_def={ "temple_def" 198 "temple_def" 197 }
 		expression_brow_wrinkles={ "brow_wrinkles_04" 73 "brow_wrinkles_04" 230 }
 		expression_eye_wrinkles={ "eye_wrinkles_03" 236 "eye_wrinkles_02" 192 }
 		expression_forehead_wrinkles={ "forehead_wrinkles_02" 253 "forehead_wrinkles_01" 161 }
 		expression_other={ "cheek_wrinkles_both_01" 0 "cheek_wrinkles_both_01" 0 }
 		complexion={ "complexion_5" 131 "complexion_5" 147 }
 		gene_height={ "normal_height" 125 "normal_height" 135 }
 		gene_bs_body_type={ "body_fat_head_fat_low" 138 "body_fat_head_fat_medium" 121 }
 		gene_bs_body_shape={ "body_shape_average_clothed" 102 "body_shape_rectangle_half" 56 }
 		gene_bs_bust={ "bust_clothes" 148 "bust_shape_3_full" 110 }
 		gene_age={ "old_2" 206 "old_1" 122 }
 		gene_eyebrows_shape={ "close_spacing_lower_thickness" 131 "avg_spacing_lower_thickness" 207 }
 		gene_eyebrows_fullness={ "layer_2_high_thickness" 148 "layer_2_avg_thickness" 128 }
 		gene_body_hair={ "body_hair_avg" 127 "body_hair_sparse" 181 }
 		gene_hair_type={ "hair_straight" 135 "hair_straight" 123 }
 		gene_baldness={ "no_baldness" 127 "no_baldness" 127 }
 		eye_accessory={ "normal_eyes" 104 "normal_eyes" 104 }
 		teeth_accessory={ "normal_teeth" 0 "normal_teeth" 0 }
 		eyelashes_accessory={ "normal_eyelashes" 219 "normal_eyelashes" 219 }
 		pose={ "" 255 "" 0 }
 		beards={ "japanese_beards" 155 "no_beard" 0 }
 		clothes={ "japanese_high_nobility_clothes" 69 "most_clothes" 0 }
 		hairstyles={ "japanese_hairstyles" 47 "all_hairstyles" 0 }
 		headgear={ "no_headgear" 196 "no_headgear" 0 }
 		legwear={ "mena_common_legwear" 238 "all_legwear" 0 }
 }
	entity={ 3942081117 3942081117 }
}

