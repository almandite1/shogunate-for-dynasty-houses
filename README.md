# Shogunate
Shogunate is a total conversion mod for Crusader Kings III. You can find more information in the [Wiki](https://gitlab.com/shogunatemod/shogunate/-/wikis/home).

![Sample Image](https://i.imgur.com/hCYAPJ8.jpg)

## About This Mod
Shogunate focuses on the medieval age in Japan, especially the Sengoku period around the 16th century. It includes a lot of historical feudal lords, aka Sengoku Daimyo, with their family trees. You can play any landed characters in the mod.

## Installation
Please read the [Wiki](https://gitlab.com/shogunatemod/shogunate/-/wikis/Installation).

## Contact Information
We develop Shogunate on [our Discord server](https://discord.gg/QqfQveq). We welcome your questions, suggestions and also participation as co-developers.

## Credit
Shogunate is based on the famous CK2 mod [Nova Monumenta Iaponiae Historica](https://steamcommunity.com/sharedfiles/filedetails/?id=333442855) with its author's permission. Special thanks to chatnoir17 and co-developers. The music comes from [PeriTune](https://peritune.com/). The thumbnail image comes from [Geospatial Information Authority of Japan](https://kochizu.gsi.go.jp/items/185).
